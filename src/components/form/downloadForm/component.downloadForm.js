import controller from './controller.downloadForm.js';
import template from './template.html';
import './style.css';

export default function downloadFormComponent() {
  return {
    controller: controller.name,
    controllerAs: 'ctrlDownload',
    template
  };
}

export default function DownloadFormController(fileService) {
  'inject';

  this.file = fileService.file;

  this.downloadFile = () => {
    const { fileName } = this.file;

    if (!fileName) {
      return;
    }

    fileService.downloadFile(fileName);
  };
}

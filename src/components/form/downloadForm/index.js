import component from './component.downloadForm.js';
import controller from './controller.downloadForm.js';
import './style.css';

export default function downloadForm(app) {
  app
    .controller(controller.name, controller)
    .component('download-form', component);
}

import controller from './controller.uploadForm.js';
import template from './template.html';

export default function uploadFormComponent() {
  return {
    controller: controller.name,
    controllerAs: 'ctrlUpload',
    template
  };
}

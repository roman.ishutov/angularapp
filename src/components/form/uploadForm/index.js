import component from './component.uploadForm.js';
import controller from './controller.uploadForm.js';
import './style.css';

export default function uploadForm(app) {
  app
    .controller(controller.name, controller)
    .component('upload-form', component);
}

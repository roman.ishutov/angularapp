export default function uploadFormController(fileService) {
  'inject';

  this.labelValue = 'Select file';


  this.uploadFile = () => {
    if (this.labelValue === 'Select file') {
      return;
    }

    const form = new FormData();
    form.append('sampleFile', this.file);
    fileService.uploadFile(form);
  };
}

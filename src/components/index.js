export { default as initFilesList } from './filesList/index';
export { default as initDownloadForm } from './form/downloadForm/index';
export { default as initUploadForm } from './form/uploadForm/index';
export { default as image } from './image/index';

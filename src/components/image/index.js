import component from './component.image.js';
import controller from './controller.image.js';
import './style.css';

export default function image(app) {
  app
    .controller(controller.name, controller)
    .component('image-wrapper', component);
}

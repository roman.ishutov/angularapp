import controller from './controller.image.js';
import template from './template.html';

export default function pictureComponent() {
  return {
    controller: controller.name,
    controllerAs: 'ctrlImage',
    template
  };
}

import component from './component.list.js';
import controller from './controller.list.js';
import './style.css';

export default function fileList(app) {
  app
    .controller(controller.name, controller)
    .component('component-list', component);
}

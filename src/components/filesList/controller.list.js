export default function FileListController(fileService) {
  'inject';
  this.filesList = fileService.getFilesList();

  this.file = fileService.file;

  this.deleteFile = value => {
    fileService.deleteFile(value);
  };
  this.selectItem = value => {
    fileService.setSelected(value);
  };
  fileService.loadFilesList();
}

import controller from './controller.list.js';
import template from './template.html';

export default function fileListComponent() {
  return {
    controller: controller.name,
    controllerAs: 'ctrlList',
    template
  };
}

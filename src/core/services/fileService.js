export default function fileService($http) {
  'inject';

  const filesList = [];
  const file = {};

  function saveFile(url, name) {
    const downloadLink = document.createElement('a');


    downloadLink.href = url;
    downloadLink.download = `${name}`;
    downloadLink.click();
  }


  function getFilesList() {
    return filesList;
  }

  function setSelected(value) {
    file.fileName = value;
  }


  function loadFilesList() {
    $http.get('/list')
      .then(res => {
        filesList.length = 0;
        filesList.push(...res);
      });
  }

  function uploadFile(data, config) {
    $http.post('upload/', data, { responseType: 'blob', ...config })
      .then(loadFilesList);
  }

  function downloadFile(fileName, config) {
    $http.get(`files/${fileName}`, { responseType: 'blob', ...config })
      .then(res => {
        const url = URL.createObjectURL(res);

        if (res.type.slice(0, 5) === 'image') {
          file.url = url;
          return;
        }

        saveFile(url, fileName);
      });
  }


  function deleteFile(data, config) {
    $http.delete(`list/${data}`, { responseType: 'text', ...config })
      .then(res => {
        const index = filesList.map(item => item.name).indexOf(res);
        filesList.splice(index, 1);
      });
  }


  return {
    file,
    setSelected,
    getFilesList,
    loadFilesList,
    uploadFile,
    downloadFile,
    deleteFile
  };
}

import angular from 'my-angular-lib';
import * as configs from '@config/index';
import * as services from '@services/index';
import * as components from '@components/index';
import './style.css';

const app = angular.module('myApp');

Object.entries(services).forEach(services => app.service(...services));
Object.values(configs).forEach(configs => app.config(configs));

Object.values(components).forEach(components => components(app));
